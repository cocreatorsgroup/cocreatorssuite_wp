<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wwwdb_v3' );

/** MySQL database username */
define( 'DB_USER', 'pmauser01' );

/** MySQL database password */
define( 'DB_PASSWORD', 'P@ssw0rd' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         't3*X!D}[0Yb,gENi2&5}A;A;[^4/~|UYe;~-2X1<cP=`ftLAaJHkjV+8I2`T*I*;' );
define( 'SECURE_AUTH_KEY',  'd4_(*jY7]r9cQRP0vNY??O5k@Nx@bP{4;y?B+MPk1dD=X*U[TTOEz6f@r!Ks opU' );
define( 'LOGGED_IN_KEY',    'R|fY8NjgzW$72EV|%6^m-ZsY{Xj&p=-QCqPdO8;6+[.IxDc_ l^Dg{3[_MJOd{$7' );
define( 'NONCE_KEY',        ']vjA20ojHXil2tne>Xr?qGV,#dKu]#[4YZo*:F.H!&_xU}nlp=]Cj,gk`gP&O.dk' );
define( 'AUTH_SALT',        'N*&9|N0_`N$Oyj@DJHv[:o^bCeA+qn9x/&7CmskNRD>= ik3C`xa q8T}S-,O{lR' );
define( 'SECURE_AUTH_SALT', 'I(J#.E4ZU`8bc.0x:(_{k:+FH:M9IXc:qir/[P&Hr2>Tg%>9x:<!=7q2G+YKx=o ' );
define( 'LOGGED_IN_SALT',   '`B9[ZyEa8}*VS/OKg8ScU$I_Ik8O^}R*;VstR9Ax[hO#o!f3$vX3^I08b[.O4W`A' );
define( 'NONCE_SALT',       ';~EkD`0eZ{dv/oA9-Io=~5dpCf7+3R>g(WQn~FWIJdx]B&8ca,b>7:np4O$vMu%x' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
define('FS_METHOD','direct');
